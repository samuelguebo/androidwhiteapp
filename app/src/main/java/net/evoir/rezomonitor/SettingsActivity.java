package net.evoir.rezomonitor;
import net.evoir.rezomonitor.fragments.SettingsFragment;

import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;


public class SettingsActivity extends PreferenceActivity {

         private Context mContext;
         private String slug;

		 @Override
		 public void onCreate(Bundle savedInstanceState) {
		  super.onCreate(savedInstanceState);
            SettingsFragment fragment = new SettingsFragment();
            mContext = this;

         // check if we received "slug"
         if (this.getIntent().getExtras() != null) {
             slug = this.getIntent().getExtras().getString("slug");

             //Toast.makeText(mContext, "On create Slug is "+slug, Toast.LENGTH_LONG).show();

             if ("bluetooth".equals(slug)) {
     //            Toast.makeText(mContext, "Slug is "+slug, Toast.LENGTH_L
                 Bundle args = new Bundle();
                 args.putString("slug", slug);

                 fragment.setArguments(args);
                 getFragmentManager().beginTransaction().replace(android.R.id.content,
                         fragment).commit();
                 PreferenceManager.setDefaultValues(SettingsActivity.this, R.xml.settings_fragment_bluetooth, false);
             } else if ("mobile".equals(slug)){
     //            Toast.makeText(mContext, "Slug is "+slug, Toast.LENGTH_L
                 Bundle args = new Bundle();
                 args.putString("slug", slug);
                 fragment.setArguments(args);
                 getFragmentManager().beginTransaction().replace(android.R.id.content,
                         fragment).commit();
                 PreferenceManager.setDefaultValues(SettingsActivity.this, R.xml.settings_fragment_mobile, false);
             }
             else {
     //            Toast.makeText(mContext, "Slug is Else", Toast.LENGTH_L
                 Bundle args = new Bundle();
                 args.putString("slug", slug);
                 fragment.setArguments(args);
                 getFragmentManager().beginTransaction().replace(android.R.id.content,
                         fragment).commit();
                 PreferenceManager.setDefaultValues(SettingsActivity.this, R.xml.settings_fragment_wifi, false);
                         }

            }
         else {
             //Toast.makeText(mContext, "Slug is EMPTY", Toast.LENGTH_LO             NG).show();

             getFragmentManager().beginTransaction().replace(android.R.id.content,
                     fragment).commit();
             PreferenceManager.setDefaultValues(SettingsActivity.this, R.xml.settings_fragment_wifi, false);
         }
		//set gradient background to action bar
	        getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_orange));
		   
		 }
  
		    @Override
		    public boolean onCreateOptionsMenu(Menu menu) {
		        // Inflate the menu; this adds items to the action bar if it is present.
		        this.getMenuInflater().inflate(R.menu.post_list, menu);
		        

		        
		        return true;
		    }
		    @Override
		    public boolean onOptionsItemSelected(MenuItem item) {

		    	if (item.getItemId() == R.id.action_back) {
		    		finish();
		    	}

		        return super.onOptionsItemSelected(item);
		    }
  	}



