package net.evoir.rezomonitor;


 

 
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.TextView;



public class SplashScreen extends Activity {
 
    private Context mContext;
    private TextView versionText,logoText, sloganText;
    private static int SPLASH_TIME_OUT = 500;
    
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		mContext = this;
		
        setContentView(R.layout.splashscreen);
        logoText = (TextView) this.findViewById(R.id.logoText);
        logoText.setText(R.string.app_name);
        
        
        versionText = (TextView) this.findViewById(R.id.versionText);
        versionText.setText(getVersion());
        
        sloganText = (TextView) this.findViewById(R.id.sloganText);
		sloganText.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));


        // Execute some code after some seconds have passed
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // shut down the activity
                finish();
                // launch home fragment
                loadHomeFragment();
            }
        }, SPLASH_TIME_OUT );

    }

    public void loadHomeFragment() {
        Intent i = new Intent(mContext, MainActivity.class);
        startActivity(i);
    }

    public String getVersion(){
		PackageManager manager = mContext.getPackageManager();
		PackageInfo info;
		String version = null;
		try {
			info = manager.getPackageInfo(mContext.getPackageName(), 0);
			version = "" + info.versionName;

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return version;
	}
	

}
