package net.evoir.rezomonitor.adapters;

import java.util.List;

import net.evoir.rezomonitor.R;
import net.evoir.rezomonitor.SettingsActivity;
import net.evoir.rezomonitor.objects.Rezo;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RezoAdapter extends BaseAdapter {

	private final List<Rezo> items;
	private Context mContext;
	private ViewHolder holder;
	public RezoAdapter(Context context, List<Rezo> items) {
		this.items = items;
		this.mContext = context;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = View.inflate(mContext, R.layout.network_list_item, null);


			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.title_item);
			holder.duration = (TextView) convertView.findViewById(R.id.duration_item);
			holder.image = (ImageView) convertView.findViewById(R.id.image_item);


			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}




		holder.title.setText(items.get(position).getTitle());
		holder.image.setImageResource(items.get(position).getImage());
		holder.duration.setText(items.get(position).getDuration());

        Button settingsButton = (Button) convertView.findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickSettings(items.get(position).getSlug());
            }
        });


        return convertView;
		}

	static class ViewHolder {
		TextView title,duration;
		ImageView image;


	}

    public void clickSettings(String slug){

        //Toast.makeText(mContext, "On RezoAdapter, slug is " + slug, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(mContext, SettingsActivity.class);
        intent.putExtra("slug", slug);
        mContext.startActivity(intent);
    }
}
