package net.evoir.rezomonitor.objects;

import java.sql.SQLException;

import net.evoir.rezomonitor.db.Model;


import android.content.Context;
import android.util.Log;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.UpdateBuilder;

public class Rezo {


	private static Dao<Rezo, String> dao;

    @DatabaseField(id = true)
	private String slug;
	@DatabaseField
    private String title;
	@DatabaseField
	private String duration;


    @DatabaseField
    private int image;
	@DatabaseField
    private int status =0;


    public Rezo() {

    }
    public Rezo(String title,String slug, int image, String duration,int status){
        //main contructor
        this.title = title;
        this.slug =slug;
        this.duration = duration;
        this.image = image;
        this.status = status;
    }
    /* Getters */

    public String getTitle() {
        return slug;
    }
    public String getSlug() {
        return slug;
    }

    public String getDuration() {
        return duration;
    }

    public int getImage() {
        return image;
    }


    public int getStatus(){
        return status;
    }

    /* Setters */

    public void setTitle(String title) {
        this.title = title;
    }
    public void setImage(int image) {
        this.image = image;
    }
    public void setStatus(int status,Context context) {
        //this.status = status;
        try {
            // we set an update builder
            dao = Model.getHelper(context).getDao(Rezo.class);
            UpdateBuilder<Rezo, String> updateBuilder = dao.updateBuilder();

            // set the criteria like you would a QueryBuilder
            updateBuilder.where().eq("slug", this.slug);
            // update the value of the fields
            updateBuilder.updateColumnValue("status",status );
            // update
            updateBuilder.update();
            this.status= status;

        } catch (SQLException e) {
            Log.e("Status changed for "+title , e.getMessage());
        }
    }
    public void setDuration(String duration,Context context) {
        try {
            // we set an update builder
            dao = Model.getHelper(context).getDao(Rezo.class);
            UpdateBuilder<Rezo, String> updateBuilder = dao.updateBuilder();

            // set the criteria like you would a QueryBuilder
            updateBuilder.where().eq("duration", this.duration);
            // update the value of the fields
            updateBuilder.updateColumnValue("duration",duration);
            // update
            updateBuilder.update();
            this.duration= duration;

        } catch (SQLException e) {
            Log.e("Duration changed for "+title , e.getMessage());
        }
    }

}
