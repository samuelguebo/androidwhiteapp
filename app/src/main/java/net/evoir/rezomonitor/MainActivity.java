package net.evoir.rezomonitor;
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.dao.Dao;


import net.evoir.rezomonitor.adapters.RezoAdapter;
import net.evoir.rezomonitor.db.Model;
import net.evoir.rezomonitor.objects.Rezo;

import org.apache.http.impl.conn.tsccm.RefQueueWorker;

import java.sql.SQLException;
import java.util.List;

public class MainActivity extends Activity {
    private ListView listView ;
    private Context mContext;
    private List<Rezo> networkList;
    private Dao<Rezo, String> dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        mContext = this;
        // Get ListView object from xml
        listView = (ListView) findViewById(R.id.networkList);
        networkList = getNetworkList();


        // Assign adapter to ListView
        if (!networkList.isEmpty()) {
            RezoAdapter adapter = new RezoAdapter(this,networkList);
            listView.setAdapter(adapter);

            // ListView Item Click Listener
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //
                }
            });

        }

    // set background color of Action bar
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.gradient_orange));

    }

    //set Listener for ListView
    private List<Rezo> getNetworkList() {
        List <Rezo> finalList = null;

        try {
            dao = Model.getHelper(mContext).getDao(Rezo.class);
            finalList = dao.queryForAll();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(finalList.isEmpty()){
            finalList.add(new Rezo("Wifi", "wifi", R.drawable.ic_wifi,"0",0)); // Add wifi
            finalList.add(new Rezo("Bluetooth", "bluetooth",R.drawable.ic_bluetooth ,"0",0)); // Add BlueTooth
            finalList.add(new Rezo("Mobile", "mobile", R.drawable.ic_mobile,"0",0)); // Add Data

        }
        return finalList;
    }

}